import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(name='Py101',
      version='0.0.1',
      author='Matt Jones',
      author_email='mjones2@allegheny.edu',
      description='Matt Jones\' py101',
      url='https://bitbucket.org/MattJones2/py101/src/master/',
      packages=setuptools.find_packages(),
      classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
      ],
)

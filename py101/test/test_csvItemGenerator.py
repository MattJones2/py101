try: #Try to import python packages
    import sys, random, pytest, os, io
    from contextlib import redirect_stdout
except ImportError as Error: #Throw an error if we can't import these packages
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    print('Error: '+str(exc_type))
    print('ErrorMsg: '+str(Error), 'in File: '+str(fname), 'on line #'+str(exc_tb.tb_lineno))
    print('\nDevMsg: Could not import required modules. If you choose not to install pytest, comment out any pytest decorator lines and remove pytest from import statement')
    sys.exit() 

scriptpath = os.path.realpath(__file__)
srcFileDir = scriptpath[:-29]+'src' #This value depends on the filename and directory name length
sys.path.append(srcFileDir)
try: #Try to import our python files/modules
    import csvItemGenerator
except ImportError as Error: #Throw an error if we can't import these files/modules
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    print('Error: '+str(exc_type))
    print('ErrorMsg: '+str(Error), 'in File: '+str(fname), 'on line #'+str(exc_tb.tb_lineno))
    print('\nDevMsg: Could not import files to test, check that they exist in a sys.path directory')
    sys.exit()

def test_baseClientData():
    """ Tests obj creation and non-variable methods in the clientData class """
    testObj = csvItemGenerator.clientData() #Create a clientData object
    assert isinstance(testObj, csvItemGenerator.clientData) #Assert that that we made the object correctly
    location = testObj.getRandomLocation() #Get random a location
    assert location in testObj.locations #Assert this is an accurate location
    status = testObj.getStatus() #Get random status
    assert status in testObj.status #Assert this is an accurate status
    accnt = csvItemGenerator.clientData.genAccountNum(10)
    assert isinstance and isinstance(accnt[0], int) and isinstance(accnt, list) and len(str(accnt[0])) == 7 and len(accnt) == 10 and len(set(accnt)) == len(accnt) #Assert that 10 unique 7-digit integers was returnd in a list
    #The below chunk of code is meant to trigger an allowed error to be printed. We redirect and capture the print statement, and assert that the error was printed
    f = io.StringIO()
    with redirect_stdout(f):
        accnt = csvItemGenerator.clientData.genAccountNum(9000001)
    assert 'Error:' in f.getvalue() and 'ErrorMsg:' in f.getvalue() and '\nDevMsg' in f.getvalue()
    name = csvItemGenerator.clientData.genName(10)
    assert isinstance(name, str) and len(name) == 10 #Assert that the name is a string, and that the length is 10

@pytest.mark.parametrize("lengthName,lengthClients", [(random.randint(10,100),random.randint(100, 100000)),(random.randint(10,100),random.randint(100, 100000)),(random.randint(10,100),random.randint(100, 100000))]) #Decorator to pass parameters to function when test is ran with pytest
def test_variableClientData(lengthName, lengthClients):
    """ Tests variable methods in the clientData class """
    name = csvItemGenerator.clientData.genName(lengthName) #Generate a random name at a certain length
    assert isinstance(name, str) and len(name) == lengthName #Assert that the name is a string, and the correct length
    accnt = csvItemGenerator.clientData.genAccountNum(lengthClients) #Generate a certain number of random account numbers
    assert isinstance and isinstance(accnt[0], int) and isinstance(accnt, list) and len(str(accnt[0])) == 7 and len(accnt) == lengthClients and len(set(accnt)) == len(accnt) #Assert that a certain number of unique 7-digit integers was returnd in a list

def test_baseGenerateClientData():
    """ Tests obj creation and non-variable methods in the generateClientData class """
    testObj = csvItemGenerator.generateClientData(1,2) #Create a generateClientData object
    assert isinstance(testObj, csvItemGenerator.generateClientData) #Assert that that we made the object correctly
    generatedData = testObj.generate() #Get random a location
    assert testObj.numberOfClients == 2 and testObj.clientNameLength == 1 and len(generatedData) == testObj.numberOfClients and len(generatedData[0]) == 5 and  generatedData[0][0] == 0 and isinstance(generatedData[0][1], str) and isinstance(generatedData[0][2], int) and isinstance(generatedData[0][3], str) and isinstance(generatedData[0][4], str)#Assert parameters are stored correctly, and that the list was returned with the correct length and correct sublist of item types

def main():
    """ Main method for manual testing """
    while(True):
        custom = input("Would you like to run a custom test?(y/n, q to quit)") #Get User input
        custom = custom.lower() #Set user input to lowercase letters
        if custom == "q":
            sys.exit() #Exit the program
        elif custom == "y":
            lengthName = int(input("Enter a name length"))
            lengthClients = int(input("Enter the desired number of clients"))
            #inputParams = True
        elif custom == "n":
            lengthName = random.randint(10,100) #Pick a random int from 10-100
            lengthClients = random.randint(100, 100000)
            #inputParams = True
        else:
            main()
        test_baseClientData()
        test_variableClientData(lengthName, lengthClients)
        test_baseGenerateClientData()
        print("done")
if __name__ == "__main__":
    main()
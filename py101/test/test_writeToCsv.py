try: #Try to import python packages
    import sys, random, pytest, os, csv, string
    from contextlib import redirect_stdout
except ImportError as Error: #Throw an error if we can't import these packages
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    print('Error: '+str(exc_type))
    print('ErrorMsg: '+str(Error), 'in File: '+str(fname), 'on line #'+str(exc_tb.tb_lineno))
    print('\nDevMsg: Could not import required modules. If you choose not to install pytest, comment out any pytest decorator lines and remove pytest from import statement')

scriptpath = os.path.realpath(__file__)
srcFileDir = scriptpath[:-23]+'src' #This value depends on the filename and directory name length
sys.path.append(srcFileDir)
try: #Try to import our python files/modules
    import csvItemGenerator
    import writeToCsv
except ImportError as Error: #Throw an error if we can't import these files/modules
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    print('Error: '+str(exc_type))
    print('ErrorMsg: '+str(Error), 'in File: '+str(fname), 'on line #'+str(exc_tb.tb_lineno))
    print('\nDevMsg: Could not import files to test, check that they exist in a sys.path directory')
    sys.exit()

def test_baseClientDataCsvWriter():
    """ Tests obj creation and non-variable methods in the clientDataCsvWriter class """
    fileName = ''.join(random.choice(string.ascii_lowercase) for i in range(5))+'.csv'
    writeObj = writeToCsv.clientDataCsvWriter([[1,2,3,4],[5,6,7,8]],fileName) #Create a clientDataCsvWriter object
    assert isinstance(writeObj, writeToCsv.clientDataCsvWriter) #Assert that we made the object correctly
    writeObj.write()#Write the csv data stored in the writeObj to a csv
    assert os.path.isfile(fileName) #Assert that the csv file exists with the correct name
    with open(fileName, 'r') as file:
        csvreader = csv.reader(file)
        i = 0
        for line in csvreader:
            if i == 0:
                assert line == ['1','2','3','4'] #Assert that line 1 of the data was written correctly
            elif i == 1:
                assert line == ['5','6','7','8'] #Assert that line 2 of the data was written correctly
            i += 1
    file.close()
    os.remove(fileName)

@pytest.mark.parametrize("fileName", [(''.join(random.choice(string.ascii_lowercase) for i in range(5))+'.csv'), (''.join(random.choice(string.ascii_lowercase) for i in range(5))+'.csv'), (''.join(random.choice(string.ascii_lowercase) for i in range(5))+'.csv')])
def test_variableClientDataCsvWriter(fileName):
    """ Tests variable methods in the clientDataCsvWriter class """
    csvRows = [] #list of rows (list of lists)
    listLength = random.randint(10,100) #Get a random number of rows for our csv.
    for i in range(listLength):
        csvRows.append([''.join(random.choice(string.printable) for k in range(random.randint(5,6))),''.join(random.choice(string.printable) for k in range(random.randint(5,6))),''.join(random.choice(string.printable) for k in range(random.randint(5,6))),''.join(random.choice(string.printable) for k in range(random.randint(5,6)))])
    writeObj = writeToCsv.clientDataCsvWriter(csvRows,fileName)
    assert isinstance(writeObj, writeToCsv.clientDataCsvWriter)
    writeObj.write()
    assert os.path.isfile(fileName)
    with open(fileName, 'r', newline = "") as file:
        csvreader = csv.reader(file)
        i = 0
        for line in csvreader:
            assert list(csvRows[i]) == line
            i += 1
    file.close()
    os.remove(fileName)

@pytest.mark.parametrize("delParam",[True])    
def test_writeAndGenerateCommunication(delParam):
    genObj = csvItemGenerator.generateClientData(25, 13) #This is already tested in other test file
    writeObj = writeToCsv.clientDataCsvWriter(genObj.generate(),'clientDataTest.csv') #This is what we really need to test
    assert isinstance(writeObj, writeToCsv.clientDataCsvWriter)
    writeObj.write()
    assert os.path.isfile('clientDataTest.csv')
    if delParam:
        os.remove('clientDataTest.csv')

def main():
    """ Main method for manual testing """
    while(True):
        userIn = input('Would you like to manually check the csv file? (y/n, press q to exit.)')
        userIn = userIn.lower()
        if userIn == 'q':
            sys.exit()
        elif userIn == 'y':
            test_baseClientDataCsvWriter()
            fileName = ''.join(random.choice(string.ascii_lowercase) for i in range(5))+'.csv'
            test_variableClientDataCsvWriter(fileName)
            test_writeAndGenerateCommunication(False)
            print("\nRemember to close and delete csv file after inspection\n")
        elif userIn == 'n':
            test_baseClientDataCsvWriter()
            fileName = ''.join(random.choice(string.ascii_lowercase) for i in range(5))+'.csv'
            test_variableClientDataCsvWriter(fileName)
            test_writeAndGenerateCommunication(True)
            
if __name__ == '__main__':
    main()
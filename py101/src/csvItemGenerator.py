""" File that generates random CSV file components to load """
import random, string,sys, os
class clientData():
    """ Class contains methods that randomly generate user input """
    def __init__(self):
        self.locations = ['imp1', 'imp2', 'imp3', 'imp4', 'imp5', 'imp6', 'imp7', 'imp8', 'imp9']
        self.status = ['outstanding', 'paid', 'pendingAction']
        
    def genName(length):
        inputs = string.printable
        return ''.join(random.choice(inputs) for i in range(length))
    
    def genAccountNum(length):
        try:
            listOfAccountNumbs = random.sample(range(1000000, 10000000), length)
            return listOfAccountNumbs
        except ValueError as Error:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print('Error: '+str(exc_type))
            print('ErrorMsg: '+str(Error), 'in File: '+str(fname), 'on line #'+str(exc_tb.tb_lineno))
            print('\nDevMsg: Sample size (ie. numberOfClients/length) larger than random number population size.')
        
    def getRandomLocation(self):
            return random.choice(self.locations)
            
    def getStatus(self):
            return random.choice(self.status)
            
            
class generateClientData(clientData):
    """ Class contains method to run clientData methods and generateID's """
    def __init__(self, clientNameLength, numberOfClients):
        clientData.__init__(self)
        self.numberOfClients = numberOfClients
        self.clientNameLength = clientNameLength
        self.listOfData = []
        self.listOfAccountNumbs = clientData.genAccountNum(numberOfClients)
    
    def generate(self):
        for i in range(self.numberOfClients):
            id = i
            name = clientData.genName(self.clientNameLength)
            acctNum = self.listOfAccountNumbs.pop(len(self.listOfAccountNumbs)-1)
            Loc = self.getRandomLocation()
            Status = self.getStatus()
            self.listOfData.append([id,name, acctNum,Loc,Status])
        return self.listOfData
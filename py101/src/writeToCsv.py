try: #Try to import python packages
    import csv
except ImportError as Error: #Throw an error if we can't import these packages
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    print('Error: '+str(exc_type))
    print('ErrorMsg: '+str(Error), 'in File: '+str(fname), 'on line #'+str(exc_tb.tb_lineno))
    print('\nDevMsg: Could not import required modules. Modules above should be a defauly module provided by python')

try: #Try to import our python files/modules
    import csvItemGenerator
except ImportError as Error: #Throw an error if we can't import these files/modules
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    print('Error: '+str(exc_type))
    print('ErrorMsg: '+str(Error), 'in File: '+str(fname), 'on line #'+str(exc_tb.tb_lineno))
    print('\nDevMsg: Could not import files to test, check that they exist in a sys.path directory')
    sys.exit()

class clientDataCsvWriter():

    def __init__(self, dataList, csvFileName):
        self.dataList = dataList
        self.csvFileName = csvFileName
   
    def write(self):
        with open(self.csvFileName, 'w+', newline='') as file:
            writer = csv.writer(file)
            writer.writerows(self.dataList)
        file.close()

def main():
    genObj = csvItemGenerator.generateClientData(25, 13)
    writeObj = clientDataCsvWriter(genObj.generate(),'clientData.csv')
    writeObj.write()

if __name__ == "__main__":
    main()
# Code Read Me for Random Csv Generator!

## Brief Description
This repository contains python files that generate a random list of items to be loaded into a csv, and that write items to a csv. It also contains a test suite that performs extensive testing on these two files.

## How to Run

### Src
To run the source code, navigate to the src file in the command prompt and type `python writeToCsv.py`

### Tests
Our test suite uses the python package Pytest. To install, type `pip install pytest` into your command prompt.

To run the test suite, navigate to the test file and type `python -m pytest`

You can also run any of the test.py files individually by typing `python testFileName.py`, and following the instructions.

## Main purpose
The main purpose of this repository is to showcase how to write OOP in python while following (most of) pythons best practices, some of pythons powerful built in packages, and how to build and use an extensive test suite in python. 

## Uses
Code can be used to generate "random client data" which we defined as a data held in a csv file which is sorted by "ID, Name, Account Number, Location, Status". You can tailor this code and test suite to generate and test similar data.

## Further Work
Since we programmed this in an OOP style, we could add a GUI class that actually called methods which took information from the user and put it into the csv (rather than randomly generated ones). It is also good practice in python to capitilize the name of your classes, which we could add to our code at a later time.
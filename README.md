# Starting with Python (For Windows)
---

## Installing Python and  Related Packages
============================================

The first step is to visit the python website, where you can find the downloads to all available python versions. We recommend using the latest version of Python 3. 


1. Visit https://python.org/downloads
2. Click the download button to download the recommended version, or choose another python version below available for download.
3. Open the Installer when ready, and make sure to check the "Add python to environment variables" selection.
4. Although we reccomend using the installation defaults by clicking "run now", you may also click "Customize Installation". If you choose a custom installation, we reccomend keeping pip, the test suite, and the add python to enviromnet variables selected. Chosing an alternate installation location may also cause problems in the future, but that's up to your discretion.
5. Allow python to be installed on your device, and wait for installation to finish. Disabling the path length limit is not necessary, but if you start getting errors like the ones found [here](https://stackoverflow.com/questions/51624449/python-setup-disabling-path-length-limit-pros-and-cons ) you may want to consider it.
6. Exit the installer and open a new instance of the command prompt.
7. Type `python -V` (shows python version) to ensure that your desired python version and it's environment variable has been set up correctly. 
8. Type `pip -V` (shows pip version) to verify that you have installed pip (PIP manages installing python packages, aka Preferred Installer Program). If you installed an older version of python (pre Python 2.7.9, 3.4), pip most likely was not installed so you should go [here](https://pip.pypa.io/en/stable/installing/ ) and follow the installation instructions to get pip (involves downloading a get-pip.py file and command line instrucitons).


**Managing PIP and other packages**

Once you verify you have python and pip, you should run `python -m pip install --upgrade pip` to ensure you have the latest version of pip installed. Run this command regularly (ie. weekly).


*Useful pip commands:*

1. pip -h (pip help)
2. pip list (lists all installed packages and their version0
3. pip list --outdated --format=columns (shows only outdated packages, their current local version and newest available version)
4. pip install <package> (installs a package locally to use in python)
5. pip uninstall <package> (uninstalls a package locally)
6. pip check <package> (verifies package has required dependencies)
7. pip -V (displays installed pip version)

*Using PIP on a proxy network*

Since pip connects to the internet to download the python packages you ask for, it won't work if you are using pip on a network that uses a proxy. To use pip, we reccomend using CNTLM or a related "proxy handler". Once you have set up the handler correctly, use the `pip --proxy http://<username>:<password>@localhost:3128 install <package> to install a package successfully over the proxy.

*Testing Packages in Python*

Many users use other large extentions such as Anaconda to perform similar functions performed by packages listed below. If disc space is not a concern and you have researched Anaconda, then that's another good option. However, we prefer to install the packages above individually, so that we don't take up unneeded disc space with "extra packages".  Click link to view package site (most packages available for download via comandline with PIP)

1. [Pipenv](https://pypi.org/project/pipenv/ ) (sets up virtual environment for your project)
2. [Memory profiler](https://pypi.org/project/memory-profiler/ ) (useful for profling memory usage while running python files)
3. [Line profiler](https://github.com/rkern/line_profiler ) (useful for profling time usage while running python files, requires Visual C++ build tools from windows visual build tools. Click [here](https://visualstudio.microsoft.com/thank-you-downloading-visual-studio/?sku=BuildTools&rel=16) to download the launcher for windows virtual build tools)
4. [IPython](https://pypi.org/project/ipython/ ) (Advanced python shell, would recommend if doing extensive python coding and testing)
5. [PyTest](https://pypi.org/project/pytest/ ) (Great package for setting up your python test suite)
---

## Using python the first time!
===============================

If you've never programmed with python before, I suggest vising your respective docs for [python3](https://docs.python.org/3/ ) or [python2](https://docs.python.org/2/ ). Here's another good [quick tutorial](https://www.thoughtco.com/quick-tutorial-on-python-2813561 ) for starting with python.


**Python shell**

The Python shell is great for testing small snippets of code before ading them to your desired .py file. The shell immedietlly runs the code that you type into it. It rembers the code you've typed in hte particular python shell instance. To launch the python shell, type `python` in your command prompt.


*Useful python shell commands*:

1. exit(), quit() (exits python shell)
2. help()
3. exec(open("fileName.py").read()) (runs the script from a python file in the shell)
*Loops in the shell*: The python shell recognizes the end of a loop with two blank lines. This allows for you to put else and elif statements one line after if statements, etc.. 

**Python cmd commands**
1. python -h (prints help message, lists commands)
2. python <fileName.py> (runs a python script from current diretory)
3. python -m <pythonPackage> <packageParams> (runs a python package if the package supports this type of call)
---

## Programming Well in Python
================================

Although I could create a whole additional readME for this section, I'll try to be concise and point out some useful tips when writing in code python.

### Organize

**Files**

Because python is a language built to facilatate testing, start your python repository by having an src (source code file) and a testing file. Your testing file should hold he python scripts responsile for testing. This scripts can easily be run when using the PyTest package.

**Code**

If you read the tutorial I linked, any python file that is being run should have a main method, and the main method should be started with an `if __name__ == __main__:` statement followed by code to run the main method. Python is a scripting langauge, but it is also an OOP language! Treat it as one by splitting the code up into methods, and clases when you want to fully take advanvtage of OOP features (inheritence, object instances, etc.).
For an example, view the code provided in this repository which showcases randomized file reading, writing (with cetain parameters/constaints in an OOP way), and testing in python. View the codeReadME to see what a normal readME might look like for this code.

COMMENT YOUR CODE! Python has two built in commenting methods, a docstring `""" Comment """` and inline comments `#Comment`. Docstrings should be used at the beginning of files to explain the file's prupose. You'll also often see them at the beginning of methods/functions. Inline comments are often used for describing technical parts of code.

### Test as You Develop

While you develope small pieces of your code bit by bit, you should also start to develop the basic tests for this code to ensure you're really developing correct code. Later, you should brainstorm extreme situations to test and finally create a random test generator to try to find more cases that you didn't think of initially.

Typical test/developing flow is:

1. Test Code in Shell --> add this test to your appropriate test_ file in test directory --> add tested code to python file.
2. Explore extreme cases to test -->add this test to your appropriate test_ file in test directory.
3. Create random generated test to test --> add this test to your appropriate test_ file in test directory.

### Things to Keep in Mind When Testing and Developing in Python

Accuracy, consistancy and time are all crucial to developing and testing code. Memory usage is followed shortly after and directly impacts the three.

Algorthim analysis is crucial to finding ways to improve your code. In general, the list of complexities from best to worst are as follows: O(1), O(n), O(log(n)n), O(n^2), O(K)

All of pythons data structures are great for different uses, but in terms of least memory used they are as follows: numpy record array, tuple, named tuple/ class with _slots_ (take up roughly the same memory), list, class, dict.

List and string manipulation: Notice the complexities found [here](https://wiki.python.org/moin/TimeComplexity ) related to maniulating strings and list (in particular, splices for strings and inserts for lists). When manipulating a list, ask if your manipulation effects the other elements in the list (ie. does it shift other elements indexes?). Notice the complexity of length, and when accessing items at either end of double-ended queues. **Side-note for lists**: while splices are expensive, slices of nupy arrays are significantly less expensive.

Python DOES NOT support multithreading, but it DOES support multi processing! Code parellelism in python accordingly.

Python-related files (files that make up the langauge, and are added such as packages, mods, etc.) can be found in the %userprofile%\AppData\Roaming\Python\<PythonVersion> directory on Windows 10